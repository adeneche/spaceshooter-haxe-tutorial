package ;

import org.flixel.FlxState;
import org.flixel.FlxText;
import org.flixel.FlxG;
import org.flixel.FlxU;

class PlayState extends FlxState 
{
	private var debug:FlxText;
	private var controls:FlxText;

	override public function create():Void {
		Registry.init();

		controls = new FlxText(0, 0, 320, "Press Control to Fire! ---------- Press 1 / 2 / 3 to change Fire Type!");
		debug = new FlxText(0, 10, 200, "");

		add(Registry.stars);
		add(Registry.bullets);
		add(Registry.ennemies);
		add(Registry.fx);
		add(Registry.player);
        add(debug);
        add(controls);
            
	}

	override public function update():Void {
		debug.text = "Bullet Pool: " + Registry.bullets.countLiving() + "/" + Registry.bullets.members.length;

		if (FlxG.keys.justPressed("ONE")) {
			Registry.player.fireType = 1;
		}
		if (FlxG.keys.justPressed("TWO")) {
			Registry.player.fireType = 2;
		}
		if (FlxG.keys.justPressed("THREE")) {
			Registry.player.fireType = 3;
		}

		FlxG.overlap(Registry.bullets, Registry.ennemies, Registry.ennemies.bulletHitEnnemy);
		
		super.update();
	}
}