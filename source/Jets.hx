package ;

import nme.display.Bitmap;
import nme.display.BitmapData;
import nme.geom.Rectangle;

/**
 * ...
 * @author haden
 */
class Jets extends Bitmap
{

	public function new() 
	{
		super(new BitmapData(3, 1, false, 0x000000));
		
		bitmapData.fillRect(new Rectangle(0,0,1,1), 0xFFFF00);
		bitmapData.fillRect(new Rectangle(1,0,1,1), 0xFF8000);
		bitmapData.fillRect(new Rectangle(2,0,1,1), 0xFF0000);
	}
	
}