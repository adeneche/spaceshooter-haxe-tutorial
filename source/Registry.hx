package;

import org.flixel.FlxSprite;
import org.flixel.plugin.photonstorm.FlxSpecialFX;
import org.flixel.FlxG;
import org.flixel.plugin.photonstorm.fx.StarfieldFX; 
 
class Registry
{
	public static var stars:FlxSprite;
	public static var player:Player;
	public static var bullets:BulletManager;
	public static var ennemies:EnnemyManager;
	public static var fx:Fx;
	
	public static function init() 
	{
		if (FlxG.getPlugin(FlxSpecialFX) == null) {
			FlxG.addPlugin(new FlxSpecialFX());
		}
		var starfield = FlxSpecialFX.starfield();
		stars = starfield.create(0, 0, FlxG.width, FlxG.height, 300);
		starfield.setStarSpeed ( 0, 1);

		player = new Player();

		bullets = new BulletManager();
		ennemies = new EnnemyManager();
		fx = new Fx();
	}
}