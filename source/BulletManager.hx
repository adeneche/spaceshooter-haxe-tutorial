package ;

import org.flixel.FlxGroup;

/**
 * ...
 * @author ...
 */
class BulletManager extends FlxGroup
{

	public function new(poolSize:Int = 40) 
	{
		super();
		
		var i = 0;
		while (i < poolSize) {
			add(new Bullet());
			i++;
		}
	}
	
	public function fire(bx:Int, by:Int):Void {
		if (getFirstAvailable() != null) {
			var bullet = cast( getFirstAvailable(), Bullet);
			bullet.fire(bx, by);
		}
	}
	
}