package ;

import org.flixel.FlxG;
import org.flixel.FlxGroup;
import nme.Lib;
import org.flixel.FlxObject;

/**
 * ...
 * @author ...
 */
class EnnemyManager extends FlxGroup
{
	private var lastReleased:Int;
	private var releaseRate:Int;
	
	public function new() 
	{
		super();
		
		releaseRate = 500;
		
		var poolSize:Int = 100;
		var i = 0;
		while (i < poolSize) {
			add(new Ennemy());
			i++;
		}
	}
	
	public function release():Void {
		var ennemy = cast( getFirstAvailable(), Ennemy);
		if (ennemy != null) {
			ennemy.launch();
		}
	}
	
	override public function update():Void {
		super.update();
		
		if (Lib.getTimer() > lastReleased + releaseRate) {
			lastReleased = Lib.getTimer();
			release();
		}
	}
	
	public function bulletHitEnnemy(bullet:FlxObject, ennemy:FlxObject):Void {
		bullet.kill();
		ennemy.hurt(1);
		Registry.fx.explodeBlock(Std.int(ennemy.x + ennemy.width * .5), Std.int(ennemy.y + ennemy.height * .5));
		FlxG.score += 1;
	}
	
}