package ;

import org.flixel.FlxSprite;
import org.flixel.FlxG;
import nme.Lib;

class Player extends FlxSprite
{
	private var bulletDelay:Int = 125;

	private var lastFired:Int;

	private var xSpeed:Float = 200;
	private var ySpeed:Float = 100;

	public var fireType:Int = 1;

	public function new()
	{
		super(FlxG.width/2, FlxG.height-16, "assets/gfx/player.png");
	}

	override public function update():Void {
		super.update();

		velocity.x = 0;
		velocity.y = 0;

		if (FlxG.keys.LEFT && x > 0) {
			velocity.x -= xSpeed;
		}

		if (FlxG.keys.RIGHT && x < (FlxG.width - width)) {
			velocity.x += xSpeed;
		}

		if (FlxG.keys.UP && y >= 100) {
			velocity.y -= ySpeed;
			if (y < 100) y = 100;
		}

		if (FlxG.keys.DOWN && y < (FlxG.height - height)) {
			velocity.y += ySpeed;
		}

		if (x < 0) {
			x = 0;
		} else if (x > FlxG.width - width) {
			x = FlxG.width - width;
		}

		if (FlxG.keys.CONTROL && Lib.getTimer() > lastFired + bulletDelay) {
			switch (fireType)
            {
                case 1:
                // Lame Single fire
                    Registry.bullets.fire(Std.int(x + 5), Std.int(y) );
 
                case 2:
                //  Double fire!
                    Registry.bullets.fire(Std.int(x), Std.int(y));
                    Registry.bullets.fire(Std.int(x + 10), Std.int(y));
 
                case 3:
                //  Insane Quad fire!
                //Notice how a 40 limit on the Bullet manager now isnt enough!! :)
                    Registry.bullets.fire(Std.int(x - 8), Std.int(y));
                    Registry.bullets.fire(Std.int(x), Std.int(y - 4));
                    Registry.bullets.fire(Std.int(x + 10), Std.int(y - 4));
                    Registry.bullets.fire(Std.int(x + 18), Std.int(y));
            }

            lastFired = Lib.getTimer();
		}
	}
}