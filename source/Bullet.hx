package ;

import org.flixel.FlxSprite;

/**
 * ...
 * @author ...
 */
class Bullet extends FlxSprite
{
	public var damage:Int = 1;
	public var speed:Int = 100;
	
	public function new() 
	{
		super(0, 0, "assets/gfx/bullet.png");
		exists = false;
	}
	
	public function fire(bx:Int, by:Int):Void {
		x = bx;
		y = by;
		
		velocity.y = -speed;
		exists = true;
	}
	
	override public function update():Void 
	{
		super.update();
		
		if (exists && y < -height) {
			exists = false;
		}
	}
	
}